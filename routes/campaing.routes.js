const {Router} = require('express');
const { 
    getCampaign, 
    updateCampaign, 
    getCampaigns, 
    removeCampaign, 
    addNewCampaign 
} = require('../controllers/campaign')
const router = Router();

router.get('/:id', getCampaign);
router.put('/:id',updateCampaign);
router.delete('/:id',removeCampaign);
router.get('/', getCampaigns);
router.post('/', addNewCampaign);

module.exports = router;