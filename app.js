const express = require('express');
const config = require('config');
const mongoose = require('mongoose');
const app = express();
const cors = require('cors');
const PORT = config.get('port') || 5000;
app.use(cors());
const mongoURI = process.env.NODE_ENV ? process.env.NODE_ENV : config.get('mongoUri');
app.use(express.json());
app.use('/api/campany', require('./routes/campaing.routes'));
async function start() {
    try {
        await mongoose.connect(mongoURI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        })
        app.listen(PORT, () => console.log(`App has been started on port ${PORT}...`))
    }
    catch (e) {
        console.log('Server Error', e.message);
        process.exit(1);
    }
}
start()