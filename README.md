## Waitstaff-campaign-backend
This is a simple CRUD server based on: node JS,express,  mongoDB.
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in.<br>
Open [http://localhost:5000](http://localhost:5000) to view it in the browser.

### `npm server`
Launches the nodemon server in the watch mode.
The page will reload if you make edits.
You will also see any lint errors in the console.
<br>

## Start the project
To start the project you must start local mongo database and then specify the parameters of the local database in the default.json file.



## Endpoints
This API have these endpoint's:

1) Get one campaign by id: 
```http
GET /api/campaign/id
```
Parameters:

| Parameter | Type | Description |
| :--- | :--- | :------ |
| `id` | `string` | **Required**. Campaign id |

Response:

| Body | Type | Description |
| :--- | :--- | :------ |
| `campaign` | `object` | Campaign in database |

Error:

| Body | Type | Description |
| :------------- | :----------- | :----------------- |
| `message` | `string` | Message with details of error |

2) Delete one campaign by id: 
```http
DELETE /api/campaign/id
```
Parameters:

| Parameter | Type | Description |
| :--- | :---| :------ |
| `id` | `string` | **Required**. Campaign id |


Response:

| Body | Type | Description |
| :--- | :--- | :------ |
| `message` | `string` | Message with details of request |

Error:

| Body | Type | Description |
| :--- | :--- | :------ |
| `message` | `string` | Message with details of error |


3) Update one campaign by id: 
```http
PUT /api/campaign/id
```
Parameters:

| Parameter | Type | Description |
| :--- | :--- | :------ |
| `id` | `string` | **Required**. Campaign id |

Response:

| Body | Type | Description |
| :--- | :--- | :----- |
| `message` | `string` | Message with details of request |

Error:

| Body | Type | Description |
| :--- | :--- | :------ |
| `message` | `string` | Message with details of error |

4) Add new campaign: 
```http
POST /api/campaign/
```
Parameters:

| Parameter | Type | Description |
| :--- | :--- | :------ |
| `req` | `object` | **Required**. Object with params that defined on the next line |

Request params:

```javascript
{
  {
      {
        "numOfDays": [
            {
                "value": number,
                "isOpen": boolean
            }
        ],
        "itemCategories": string,
        "minItemCount": [
            {
                "value": number,
                "isOpen": boolean
            }
        ],
        "discountPrice": [
            {
                "value": number,
                "isOpen": boolean
            }
        ],
        "campaignName": string (required),
        "discountType": string (required),
        "validTill": Date (required),
        "totalRedemptions": number,
        "isCategoryMenuOpen": boolean,
    },
}
}
```
Response:

| Body | Type | Description |
| :--- | :--- | :------ |
| `message` | `string` | Message with details of request |
| `id` | `string` | Id of added item|

```javascript
{
  message: "Campaign with id has successfully added"
}
```

Error:

| Body | Type | Description |
| :--- | :--- | :------ |
| `message` | `string` | Message with details of error |


```javascript
{
  message: "There are problems with adding new campaign. Error: error"
}
```

5) Get all campaign: 
```http
GET /api/campaign/
```
Parameters:

| Parameter | Type | Description |
| :--- | :--- | :------- |
| no params |    |    |


Response:

| Body | Type | Description |
| :------- | :-------- | :------------------- |
| `[{company}]` | `List of Objects` | List with all of the Campaign in the database |

Response sample:

```javascript
[{
        "numOfDays": [
            {
                "value": 10,
                "isOpen": true
            }
        ],
        "itemCategories": "Burritoos",
        "minItemCount": [
            {
                "value": 10,
                "isOpen": true
            }
        ],
        "discountPrice": [
            {
                "value": 10,
                "isOpen": true
            }
        ],
        "_id": "5ee1cf58fbad516f8e05fc30",
        "campaignName": "Test campain 2",
        "discountType": "BOGO Discount",
        "validTill": "2020-06-21T13:36:29.061Z",
        "totalRedemptions": 0,
        "isCategoryMenuOpen": true,
        "createdAt": "2020-06-11T06:29:44.157Z",
        "updatedAt": "2020-06-11T06:29:44.157Z",
        "__v": 0
},..]
```
Error:

| Body | Type | Description |
| :----- | :---- | :---------- |
| `message` | `string` | Message with details of error |

## Status Codes

Waitstaff-backend returns the following status codes in its API:

| Status Code | Description |
| :--- | :--- |
| 200 | `OK` |
| 201 | `CREATED` |
| 400 | `BAD REQUEST` |
| 404 | `NOT FOUND` |
| 500 | `INTERNAL SERVER ERROR` |