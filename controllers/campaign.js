const Campaign = require('../models/Campaign');
const getCampaign = async (req, res) => {
        try {
            const {id} = req.params;
            const campaign = await Campaign.findOne({_id: id});
            if (campaign) return res.json(campaign);
            return res.status(404).json({
                message: `This campaign with id:${id} are not exist in the database`
            });
        } catch (error) {
            res.status(500).json({
                message: `Can not get campaign with id: ${id}. Problems on a server error: ${error}`
            });
        }
}
const updateCampaign = async (req, res) => {
        try {
            const { id } = req.params;
            const {...data} = req.body;
            const campaign = await Campaign.updateOne({_id: id}, {...data});
            if (campaign) return res.status(200).json({
                message: `Campaign with id: ${id} has successfully updated`
            });
            return res.status(404).json({
                message: `Error campaign with id: ${id} has not been updated`
            });
        } catch (error) {
            res.status(500).json({
                message: `The campaign with the id of:${id} has not been updated error: ${error}`
            });
        }
}
const removeCampaign = async (req, res) => {
        try {
            const {id} = req.params;
            const campaign = await Campaign.deleteOne({_id: id});
            if (campaign.deletedCount) return res.json(id);
            return res.status(404).json({
                message: `Campaign with id: ${id} has successfully deleted`
            });
        } catch (error) {
            res.status(500).json({
                message: `Could not delete campaign with id ${id}. ${error}`
            })
        }
}
const getCampaigns = async (req, res) => {
        try {
            const campaigns = await Campaign.find();
            if (campaigns) return res.json(campaigns);
            return res.status(404).json({
                message: `There are problems with database try to make request again`
            })
        } catch (error) {
            res.status(500).json({
                message: `Request error. Error: ${error}`
            })
        }
}
const addNewCampaign = async (req, res) => {
        try {
            const {
                campaignName, 
                discountType, 
                validTill, 
                numOfDays, 
                itemCategories, 
                minItemCount, 
                discountPrice,
                totalRedemptions, 
                isCategoryMenuOpen 
              } = req.body;
            const campaign = new Campaign({
                campaignName, 
                discountType, 
                validTill, 
                numOfDays, 
                itemCategories, 
                minItemCount, 
                discountPrice,
                totalRedemptions, 
                isCategoryMenuOpen 
            });
            const result = await campaign.save();
            if (res) return res.status(201).json({
                id: result.id,
                message: `Campaign with id:${result.id} has successfully added`
            })
            return res.status(404).json({
                message: `There are problems with adding this campaign with id:${result.id}. 
                Please try to check incoming arguments`
            })
        } catch (error) {
            res.status(500).json({
                message: `There are problems with adding new campaign with id: ${result.id}. Error: ${error}`
            })
        }
}
module.exports = {
  getCampaign,
  updateCampaign,
  getCampaigns,
  removeCampaign,
  addNewCampaign,
}